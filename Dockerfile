FROM openjdk:8

WORKDIR /opt/netrist
COPY build/libs/naics-web-service-0.0.1-SNAPSHOT.jar /opt/netrist/naics-web-service-0.0.1-SNAPSHOT.jar

EXPOSE 8080

CMD ["java", "-jar", "naics-web-service-0.0.1-SNAPSHOT.jar"]
