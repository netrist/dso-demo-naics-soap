# DSO Demo NAICS SOAP

SOAP NAICS look-up service


# About the DevSecOps Pipeline

This project uses a collection of tools to run the code and resulting service artfacts through quality gates. The quality gates are excercised **prior to merging code** so that the peer review of the merge request cannot proceed until the quality of the request is considered high.

### Stages run on every Merge Request:

**Build** - compiles, unit tests, and builds the Java code. uses [jUnit](https://junit.org/junit5/), a popular Java unit testing framework, to fully unit test the Java code

**Code Coverage** - uses Sonarcloud.io (SonarQube) [https://sonarcloud.io/](https://sonarcloud.io/) to analyze the code and measure it against internal standards for unit test line coverage, security, code styles, etc

**Application Security** - this phase starts the web service and uses the Zed Attack Proxy [https://www.zaproxy.org/](https://www.zaproxy.org/) to passively scan the web app for top vulnverabilities identified by the Open Web Application Security Project (**OWASP)**

**Build Container** - this phase builds a Docker container and pushes it to the correct repository URL


### Deployment:

**Deploy to Kubernetes** - for this example, we proactively deploy the approved image to the Kubernetes cluster using a rolling restart (zero downtime) and tag the image repository to make roll-back simple. 

However, the evolving best practice is to use GitOps to update the definition of the deployed platform (Kubernetes specifications) and allow a Kubernetes observer such as ArgoCD to compare the state of the cluster against a good configuration 
