#!/bin/bash


githash=$(git rev-parse HEAD | cut -c1-8)

sed 's!GITHASH!'"$githash"'!' ./soap-project/GenericNAICS-soapui-project.xml > ./soap-project/GenericNAICS-soapui-project-updated.xml 
