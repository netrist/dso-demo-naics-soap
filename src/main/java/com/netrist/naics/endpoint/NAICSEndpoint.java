package com.netrist.naics.endpoint;

import com.netrist.naics.generated.*;
import com.netrist.naics.repository.NAICSRepository;
import com.netrist.naics.service.NAICSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class NAICSEndpoint {
    private static final String NAMESPACE_URI = "http://example.netrist.com/";

    @Autowired
    private NAICSRepository naicsRepository;
    
    @Autowired 
    private NAICSService naicsService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetNAICSByID")
    @ResponsePayload
    public GetNAICSByIDResponse getNAICSByID(@RequestPayload GetNAICSByID request) {
        GetNAICSByIDResponse response = new GetNAICSByIDResponse();

        NAICSList naics = naicsRepository.getNAICSByID(request.getNAICSCode());

        if(naics != null && naics.getNAICSData() != null && naics.getNAICSData().getNAICS() != null
                && !naics.getNAICSData().getNAICS().isEmpty()) {
            response.setGetNAICSByIDResult(true);
        }
        response.setNAICSData(naics);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetNAICSByGroup")
    @ResponsePayload
    public GetNAICSByGroupResponse getNAICSByGroup(@RequestPayload GetNAICSByGroup request) {
        GetNAICSByGroupResponse response = new GetNAICSByGroupResponse();

        NAICSList naics = naicsRepository.getNAICSByGroup(request.getGroupCode());

        if(naics != null && naics.getNAICSData() != null && naics.getNAICSData().getNAICS() != null
                        && !naics.getNAICSData().getNAICS().isEmpty()) {
            response.setGetNAICSByGroupResult(true);
        }
        response.setNAICSData(naics);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetNAICSByIndustry")
    @ResponsePayload
    public GetNAICSByIndustryResponse getNAICSByIndustry(@RequestPayload GetNAICSByIndustry request) {
        GetNAICSByIndustryResponse response = new GetNAICSByIndustryResponse();

        NAICSList naics = naicsRepository.getNAICSByIndustry(request.getIndustryCode());

        if(naics != null && naics.getNAICSData() != null && naics.getNAICSData().getNAICS() != null
                && !naics.getNAICSData().getNAICS().isEmpty()) {
            response.setGetNAICSByIndustryResult(true);
        }
        response.setNAICSData(naics);

        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetServiceIdentifier")
    @ResponsePayload
    public GetServiceIdentifierResponse getVersion(@RequestPayload GetServiceIdentifier request) {
        GetServiceIdentifierResponse response = new GetServiceIdentifierResponse();

        String gitCommitID = naicsService.getNAICManifestGitCommitID();
        
        response.setServiceIdentifier(gitCommitID);

        return response;
    }

}
