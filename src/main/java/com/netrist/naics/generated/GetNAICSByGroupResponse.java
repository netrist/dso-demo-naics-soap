
package com.netrist.naics.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetNAICSByGroupResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NAICSData" type="{http://example.netrist.com/}NAICSList"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getNAICSByGroupResult",
    "naicsData"
})
@XmlRootElement(name = "GetNAICSByGroupResponse")
public class GetNAICSByGroupResponse {

    @XmlElement(name = "GetNAICSByGroupResult")
    protected boolean getNAICSByGroupResult;
    @XmlElement(name = "NAICSData", required = true)
    protected NAICSList naicsData;

    /**
     * Gets the value of the getNAICSByGroupResult property.
     * 
     */
    public boolean isGetNAICSByGroupResult() {
        return getNAICSByGroupResult;
    }

    /**
     * Sets the value of the getNAICSByGroupResult property.
     * 
     */
    public void setGetNAICSByGroupResult(boolean value) {
        this.getNAICSByGroupResult = value;
    }

    /**
     * Gets the value of the naicsData property.
     * 
     * @return
     *     possible object is
     *     {@link NAICSList }
     *     
     */
    public NAICSList getNAICSData() {
        return naicsData;
    }

    /**
     * Sets the value of the naicsData property.
     * 
     * @param value
     *     allowed object is
     *     {@link NAICSList }
     *     
     */
    public void setNAICSData(NAICSList value) {
        this.naicsData = value;
    }

}
