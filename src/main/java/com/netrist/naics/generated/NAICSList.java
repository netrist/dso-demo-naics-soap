
package com.netrist.naics.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NAICSList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NAICSList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Records" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NAICSData" type="{http://example.netrist.com/}ArrayOfNAICS" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NAICSList", propOrder = {
    "records",
    "naicsData"
})
public class NAICSList {

    @XmlElement(name = "Records")
    protected int records;
    @XmlElement(name = "NAICSData")
    protected ArrayOfNAICS naicsData;

    /**
     * Gets the value of the records property.
     * 
     */
    public int getRecords() {
        return records;
    }

    /**
     * Sets the value of the records property.
     * 
     */
    public void setRecords(int value) {
        this.records = value;
    }

    /**
     * Gets the value of the naicsData property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfNAICS }
     *     
     */
    public ArrayOfNAICS getNAICSData() {
        return naicsData;
    }

    /**
     * Sets the value of the naicsData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfNAICS }
     *     
     */
    public void setNAICSData(ArrayOfNAICS value) {
        this.naicsData = value;
    }

}
