
package com.netrist.naics.generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.netrist.example package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetNAICSByID_QNAME = new QName("http://example.netrist.com/", "GetNAICSByID");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.netrist.example
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetServiceIdentifierResponse }
     * 
     */
    public GetServiceIdentifierResponse createGetServiceIdentifierResponse() {
        return new GetServiceIdentifierResponse();
    }

    /**
     * Create an instance of {@link GetNAICSByIndustry }
     * 
     */
    public GetNAICSByIndustry createGetNAICSByIndustry() {
        return new GetNAICSByIndustry();
    }

    /**
     * Create an instance of {@link GetServiceIdentifier }
     * 
     */
    public GetServiceIdentifier createGetServiceIdentifier() {
        return new GetServiceIdentifier();
    }

    /**
     * Create an instance of {@link GetNAICSByGroup }
     * 
     */
    public GetNAICSByGroup createGetNAICSByGroup() {
        return new GetNAICSByGroup();
    }

    /**
     * Create an instance of {@link GetNAICSByGroupResponse }
     * 
     */
    public GetNAICSByGroupResponse createGetNAICSByGroupResponse() {
        return new GetNAICSByGroupResponse();
    }

    /**
     * Create an instance of {@link NAICSList }
     * 
     */
    public NAICSList createNAICSList() {
        return new NAICSList();
    }

    /**
     * Create an instance of {@link NAICSCodeType }
     * 
     */
    public NAICSCodeType createNAICSCodeType() {
        return new NAICSCodeType();
    }

    /**
     * Create an instance of {@link GetNAICSByIDResponse }
     * 
     */
    public GetNAICSByIDResponse createGetNAICSByIDResponse() {
        return new GetNAICSByIDResponse();
    }

    /**
     * Create an instance of {@link GetNAICSByIndustryResponse }
     * 
     */
    public GetNAICSByIndustryResponse createGetNAICSByIndustryResponse() {
        return new GetNAICSByIndustryResponse();
    }

    /**
     * Create an instance of {@link ArrayOfNAICS }
     * 
     */
    public ArrayOfNAICS createArrayOfNAICS() {
        return new ArrayOfNAICS();
    }

    /**
     * Create an instance of {@link NAICS }
     * 
     */
    public NAICS createNAICS() {
        return new NAICS();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NAICSCodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://example.netrist.com/", name = "GetNAICSByID")
    public JAXBElement<NAICSCodeType> createGetNAICSByID(NAICSCodeType value) {
        return new JAXBElement<NAICSCodeType>(_GetNAICSByID_QNAME, NAICSCodeType.class, null, value);
    }

}
