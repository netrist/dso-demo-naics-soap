package com.netrist.naics.repository;

import com.netrist.naics.generated.ArrayOfNAICS;
import com.netrist.naics.generated.NAICS;
import com.netrist.naics.generated.NAICSList;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

@Component
public class NAICSRepository {
    private static final ArrayList<NAICS> naics = new ArrayList<>();

    /*
    When user queries GetNAICSByID with 541511, or
    queries GetNAICSByIndustry with 54151, or
    queries GetNAICSByGroup with 5415, this record is the result
         <exam:NAICSData>
            <exam:Records>1</exam:Records>
            <!--Optional:-->
            <exam:NAICSData>
               <!--Zero or more repetitions:-->
               <exam:NAICS>
                  <exam:NAICSCode>541511</exam:NAICSCode>
                  <exam:Title>Custom Computer Programming Services</exam:Title>
                  <exam:Country>US</exam:Country>
                  <exam:IndustryDescription>Professional, Scientific, and Technical Services</exam:IndustryDescription>
               </exam:NAICS>
            </exam:NAICSData>
     */

    @PostConstruct
    public void initData() {
        // Load dummy data
        NAICS naics0 = new NAICS();
        naics0.setCountry("US");
        naics0.setTitle("Custom Computer Programming Services");
        naics0.setIndustryDescription("Professional, Scientific, and Technical Services");
        naics0.setNAICSCode("541511");
        naics.add(naics0);

    }

    private NAICS findNaics(String id) {
        Assert.notNull(id, "The naics' id must not be null");
        NAICS result = null;
        for(NAICS n : naics) {
            if(n.getNAICSCode().equals(id)) {
                result = n;
            }
        }
        return result;
    }

    public NAICSList getNAICSByID(String id) {
        NAICSList naicsList = new NAICSList();
        NAICS naicsResult = findNaics(id);

        if(naicsResult != null) {
            populateResult(naicsList, naicsResult);
        } else {
            naicsList.setRecords(0);
        }

        return naicsList;
    }

    public NAICSList getNAICSByIndustry(String industry) {
        // Trim group if user puts in full industry code
        if(industry.length() > 5) {
            industry = industry.substring(0, 5);
        }
        NAICSList naicsList = new NAICSList();
        naicsList.setRecords(0);

        NAICS result =null;

        for(NAICS n : naics) {
            if(n.getNAICSCode().substring(0,n.getNAICSCode().length()-1).equals(industry)) {
                result = n;
                populateResult(naicsList, result);
            }
        }

        return naicsList;
    }

    public NAICSList getNAICSByGroup(String group) {
        // Trim group if user puts in full industry code
        if(group.length() > 4) {
            group = group.substring(0, 4);
        }

        NAICSList naicsList = new NAICSList();
        naicsList.setRecords(0);

        NAICS result =null;

        for(NAICS n : naics) {
            if(n.getNAICSCode().substring(0,n.getNAICSCode().length()-2).equals(group)) {
                result = n;
                populateResult(naicsList, result);
            }
        }

        return naicsList;
    }

    private NAICSList populateResult(NAICSList naicsList, NAICS naics) {
        ArrayOfNAICS arrayOfNAICS = new ArrayOfNAICS();
        arrayOfNAICS.getNAICS().add(naics);
        naicsList.setNAICSData(arrayOfNAICS);
        naicsList.setRecords(naicsList.getNAICSData().getNAICS().size());

        return naicsList;
    }
}
