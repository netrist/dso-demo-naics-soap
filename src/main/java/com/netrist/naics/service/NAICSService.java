package com.netrist.naics.service;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

@Component
public class NAICSService {

    public static final String IMPLEMENTATION_TITLE_ATTRIBUTE = "Implementation-Title";
    public static final String GIT_COMMIT_ID_ATTRIBUTE = "Git-CommitId";
    public static final String IMPLEMENTATION_TITLE_VALUE = "NAICS";

    public String getNAICManifestGitCommitID(){
        String gitCommitId = null;
        
        //Find NAICS Manifest (This is gotten from manifest generated in gradle file)
        Manifest naicManifest = getManifestByAttributeNameAndValue(IMPLEMENTATION_TITLE_ATTRIBUTE,IMPLEMENTATION_TITLE_VALUE );
        
        if(naicManifest !=null) {
            gitCommitId = naicManifest.getMainAttributes().getValue(GIT_COMMIT_ID_ATTRIBUTE);
        }
        
        //If we can't find an ID, put all zeros 
        if(gitCommitId == null) {
            gitCommitId = "00000000";
        }
        return gitCommitId;
    }

    public Manifest getManifestByAttributeNameAndValue(String attributeName, String value) {
        HashMap manifestMap = new HashMap<String,Manifest>();

        try{
            Enumeration resources = this.getClass().getClassLoader().getResources("META-INF/MANIFEST.MF");

            while(resources.hasMoreElements()){
                try{
                    InputStream manifestInput = ((URL)resources.nextElement()).openStream();
                    Manifest manifest = new Manifest(manifestInput);
                    Attributes attributes = manifest.getMainAttributes();
                    
                    String attributeValue = attributes.getValue(attributeName);
                    if(attributeValue!=null && attributeValue.equalsIgnoreCase(value)){
                        return manifest;
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
         catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
