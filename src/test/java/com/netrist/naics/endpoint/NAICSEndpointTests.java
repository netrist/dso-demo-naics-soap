package com.netrist.naics.endpoint;

import com.netrist.naics.generated.*;
import com.netrist.naics.repository.NAICSRepository;
import com.netrist.naics.service.NAICSService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NAICSEndpointTests {
    @Mock
    NAICSService naicsService;

    @Mock
    NAICSRepository naicsRepository;

    @InjectMocks
    NAICSEndpoint naicsEndpoint = new NAICSEndpoint();

    private static final String TEST_ID = "1234abcd";

    private NAICSList returnList;

    @BeforeEach
    void setup() {
        NAICS naics = new NAICS();
        ArrayOfNAICS arrayOfNAICS = new ArrayOfNAICS();
        arrayOfNAICS.getNAICS().add(naics);

        returnList = new NAICSList();
        returnList.setNAICSData(arrayOfNAICS);
        returnList.setRecords(1);
    }

    @Test
    void getVersionTest() {
        when(naicsService.getNAICManifestGitCommitID()).thenReturn(TEST_ID);

        GetServiceIdentifierResponse response = naicsEndpoint.getVersion(new GetServiceIdentifier());

        assertEquals(TEST_ID,response.getServiceIdentifier());
    }

    @Test
    void getNAICSByIDTest() {

        GetNAICSByID request = new GetNAICSByID();
        request.setNAICSCode(TEST_ID);

        when(naicsRepository.getNAICSByID(TEST_ID)).thenReturn(returnList);

        GetNAICSByIDResponse response = naicsEndpoint.getNAICSByID(request);

        assertEquals(true, response.isGetNAICSByIDResult());
        assertEquals(returnList, response.getNAICSData());
    }

    @Test
    void getNAICSByIDNegativeTest() {

        GetNAICSByID request = new GetNAICSByID();
        request.setNAICSCode(TEST_ID);

        NAICSList negList = getNegativeReturnList();

        when(naicsRepository.getNAICSByID(TEST_ID)).thenReturn(negList);

        GetNAICSByIDResponse response = naicsEndpoint.getNAICSByID(request);

        assertEquals(true, response.isGetNAICSByIDResult());
        assertEquals(negList, response.getNAICSData());
    }

    @Test
    void getNAICSByGroupTest() {
        GetNAICSByGroup request = new GetNAICSByGroup();
        request.setGroupCode(TEST_ID);

        when(naicsRepository.getNAICSByGroup(TEST_ID)).thenReturn(returnList);

        GetNAICSByGroupResponse response = naicsEndpoint.getNAICSByGroup(request);

        assertEquals(true, response.isGetNAICSByGroupResult());
        assertEquals(returnList, response.getNAICSData());
    }

    @Test
    void getNAICSByGroupNegativeTest() {
        GetNAICSByGroup request = new GetNAICSByGroup();
        request.setGroupCode(TEST_ID);

        NAICSList negList = getNegativeReturnList();

        when(naicsRepository.getNAICSByGroup(TEST_ID)).thenReturn(negList);

        GetNAICSByGroupResponse response = naicsEndpoint.getNAICSByGroup(request);

        assertEquals(true, response.isGetNAICSByGroupResult());
        assertEquals(negList, response.getNAICSData());
    }

    @Test
    void getNAICSByIndustryTest() {
        GetNAICSByIndustry request = new GetNAICSByIndustry();
        request.setIndustryCode(TEST_ID);

        when(naicsRepository.getNAICSByIndustry(TEST_ID)).thenReturn(returnList);

        GetNAICSByIndustryResponse response = naicsEndpoint.getNAICSByIndustry(request);

        assertEquals(true, response.isGetNAICSByIndustryResult());
        assertEquals(returnList, response.getNAICSData());
    }
    @Test
    void getNAICSByIndustryNegativeTest() {
        GetNAICSByIndustry request = new GetNAICSByIndustry();
        request.setIndustryCode(TEST_ID);

        NAICSList negList = getNegativeReturnList();

        when(naicsRepository.getNAICSByIndustry(TEST_ID)).thenReturn(negList);

        GetNAICSByIndustryResponse response = naicsEndpoint.getNAICSByIndustry(request);

        assertEquals(true, response.isGetNAICSByIndustryResult());
        assertEquals(negList, response.getNAICSData());
    }


    private NAICSList getNegativeReturnList() {
        NAICSList negReturnList = new NAICSList();

        NAICS naics = new NAICS();
        ArrayOfNAICS arrayOfNAICS = new ArrayOfNAICS();
        arrayOfNAICS.getNAICS().add(naics);

        negReturnList.setNAICSData(arrayOfNAICS);

        return negReturnList;
    }

}
