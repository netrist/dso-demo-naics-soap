package com.netrist.naics.repository;

import com.netrist.naics.generated.NAICSList;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class NAICSRepositoryTests {

    @Autowired
    NAICSRepository repository;

    @Test
    void getNAICSByIDTest() throws Exception {
        String id = "541511";
        NAICSList naicsList = repository.getNAICSByID(id);

        assertNotNull(naicsList.getNAICSData());
    }

    @Test
    void getNAICSByIDNegativeTest() throws Exception {
        String id = "531511";
        NAICSList naicsList = repository.getNAICSByID(id);

        assertNull(naicsList.getNAICSData());
    }

    @Test
    void getNAICSByGroupTest() throws Exception {
        String id = "5415";
        NAICSList naicsList = repository.getNAICSByGroup(id);

       assertNotNull(naicsList.getNAICSData());
    }

    @Test
    void getNAICSByGroupFullIndustryCodeTest() throws Exception {
        String id = "54151";
        NAICSList naicsList = repository.getNAICSByGroup(id);

        assertNotNull(naicsList.getNAICSData());
    }

    @Test
    void getNAICSByGroupNegativeTest() throws Exception {
        String id = "541";
        NAICSList naicsList = repository.getNAICSByGroup(id);

        assertNull(naicsList.getNAICSData());
    }

    @Test
    void getNAICSByIndustryTest() throws Exception {
        String id = "54151";
        NAICSList naicsList = repository.getNAICSByIndustry(id);

        assertNotNull(naicsList.getNAICSData());
    }

    @Test
    void getNAICSByIndustryFullIndustryCodeTest() throws Exception {
        String id = "541511";
        NAICSList naicsList = repository.getNAICSByIndustry(id);

        assertNotNull(naicsList.getNAICSData());
    }

    @Test
    void getNAICSByIndustryNegativeTest() throws Exception {
        String id = "531511";
        NAICSList naicsList = repository.getNAICSByIndustry(id);

        assertNull(naicsList.getNAICSData());
    }
}
