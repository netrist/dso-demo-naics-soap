package com.netrist.naics.service;


import org.junit.jupiter.api.Test;

import java.util.jar.Manifest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class NAICSServiceTests {

    private NAICSService naicsService = new NAICSService();

    private String testCommitId = "abcd1234";

    @Test
    void getNAICManifestGitCommitIDNoManifestTest() throws Exception {
        String commitIDResult = naicsService.getNAICManifestGitCommitID();

        assertEquals("00000000",commitIDResult);
    }

    @Test
    void getManifestByAttributeNameAndValueTest() {

        Manifest manifest = naicsService
                .getManifestByAttributeNameAndValue(NAICSService.IMPLEMENTATION_TITLE_ATTRIBUTE,"wsdl4j");

        assertNotNull(manifest);
    }
}
